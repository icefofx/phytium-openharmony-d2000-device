#!/bin/bash
if [ $# -eq 0 ] ; then
   echo -e "\033[31m USAGE: auto_patch.sh OHOS_PATH_ROOT  \033[0m"
   echo -e "\033[31m example: ./auto_patch.sh /home/phytium/OpenHarmony  \033[0m"
   exit 1;
fi

OHOS_PATH_ROOT=$1
if [ ! -d $OHOS_PATH_ROOT ];then
    echo -e "\033[31mMake sure the OHOS_PATH_ROOT exist! \033[0m"
    exit 1;
fi

DEVICE=d2000

clean=0
if [ $# -eq 2 ];then
    if [ $2="clean" ];then
        clean=1
    fi
fi

PATCH_PATH=$OHOS_PATH_ROOT/device/board/phytium/$DEVICE/patch
if [ ! -d $PATCH_PATH ];then
    echo -e "\033[31mMake sure you have copied phytium device code to OHOS_PATH_ROOT! \033[0m"
    exit 1;
fi

echo -e ""
echo -e "######################################################################"
echo -e "#### \033[32m Auto apply phytium_oh patchs start !\033[0m"
echo -e "#### \033[32m OHOS_PATH_ROOT: $OHOS_PATH_ROOT \033[0m"
echo -e "#### \033[32m PATCH_PATH    : $PATCH_PATH \033[0m"
echo -e "#### \033[32m DEVICE        : $DEVICE \033[0m"
echo -e "######################################################################"

CURRENTPWD=$(pwd)

####get_git_path(patch_name) get git path from patch name to git apply
function get_git_path(){
    local path=$1
    path=${path#*=}
    path=${path%=*}
    path=${path//-/\/}
    echo $path
}

for file in `ls $PATCH_PATH`
do
    git_path=$(get_git_path $file)/
    if [ ! -d $OHOS_PATH_ROOT/$git_path ];then
       echo -e "\033[31m$git_path not exit \033[0m"
       continue
    fi

    cd $OHOS_PATH_ROOT/$git_path
    if [ $clean -eq 0 ];then
        echo -e "\n\033[32m#### git apply $file #### \033[0m"
        git apply $PATCH_PATH/$file
        echo -e "\033[32m#### git apply $file end#### \033[0m"
    else
        echo -e "\n\033[32m#### git reset $git_path #### \033[0m"
        git checkout ./*
        echo -e "\033[32m#### git reset $git_path end #### \033[0m"
    fi
#    git status
done

cd $CURRENTPWD

echo -e "\n#### \033[32mAuto apply phytium_oh patchs end !\033[0m"
