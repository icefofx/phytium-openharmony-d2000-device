# Phytium-OpenHarmony-D2000-device
##  Intro
This project introduces how to run OpenHarmony standard system ([ver 3.2 Release](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-release.md)) on Phytium D2000 + X100 DEV hardware platform from Phytium Technology Co., Ltd.  
It supports video decoding hardware acceleration and graphics display hardware acceleration provided by the X100.  
It supports Linux kernel 5.10.

# How to set up the development environment
## 1.1 Hardware environment
A X86 host with the ubuntu20.04 OS installed is needed. The minimum memory requirement is 16 GB.
## 1.2 Download repo script file
1. Register the [gitee](https://gitee.com/signup?redirect_to_url=%2Fdashboard) account.

2. Register the gitee SSH public key, please refer to[Gitee help info](https://gitee.com/help/articles/4191).

3. Install [git client](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git) and [git-lfs](https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading), and then configure the git user information.
```
git config --global user.name "your-name"
git config --global user.email "your-email-address"
git config --global credential.helper store
```
4. Install repo tools by running the commands below.
```
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```
>Tips:  
If you do not have the permission to access /usr/local/bin/, you can download the repo to another directory and configure it into environment variables .**

## 1.3 Get OpenHarmony standard system source code
Download OpenHarmony standard system source code by repo + ssh.
```
export WORK_SPACE=/home/xxx/workspace #replace to your own workspace path
export PROJ_ROOT=$WORK_SPACE/OpenHarmony
mkdir $WORK_SPACE
mkdir $PROJ_ROOT
cd &PROJ_ROOT
repo init -u https://gitee.com/openharmony/manifest -b OpenHarmony-3.2-Release --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
## 1.4 Get the compilation toolchain
Get the compilation toolchain by using the installation package.
```
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby
```
## 1.5 Execute prebuilts script
Go to the root path of OpenHarmony source code, and execute prebuilts script to install the compiler and binary tools .
```
cd &PROJ_ROOT
bash build/prebuilts_download.sh
```

# Download and integrate phytium device source code
## 2.1 Download phytium device source code
Create a local path and download phytium device source code to it .
```
export PHY_DEV=$WORK_SPACE/phytium_device
mkdir $PHY_DEV
cd $PHY_DEV
git clone git@gitee.com:phytium_embedded/phytium-openharmony-d2000-device.git
```
## 2.2 Integrate phytium device code
1. Copy the phytium device source code to the corresponding device directory of the OpenHarmony source code.
```
cp $PHY_DEV/device_board_phytium $PROJ_ROOT/device/board/phytium -r
cp $PHY_DEV/device_soc_phytium $PROJ_ROOT/device/soc/phytium -r
cp $PHY_DEV/vendor_phytium $PROJ_ROOT/vendor/phytium -r
```
2. Execute $PHY_DEV/auto_patch.sh to apply all the patches in device_board_phytium/d2000/patch/ to the corresponding repositories in OpenHarmony source code .
```
$PHY_DEV/auto_patch.sh $PROJ_ROOT
```

# Build
## 3.1 Build OpenHarmony source code
```
cd $PROJ_ROOT
./build.sh --product-name d2000 --ccache --target-cpu arm64
```
 Prompt message will be printed after building successfully:
```
post_process
=====build d2000 successful.
```
Out put files
```
$PROJ_ROOT/out/d2000/packages/phone/images/system.img
$PROJ_ROOT/out/d2000/packages/phone/images/vendor.img
$PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img
```
## 3.2 Build linux kernel
Please connect Phytium Technology Co. LTD by emails provided at the end of this file to get linux kernel source code, and build it by yourself referring to the README in linux kernel source code.
>**!!! Notice:**  
As vpu is compiled as a module of linux kernel modules, when you update linux kernel, you should update the ko files of vpu at the same time. When linux kernel project is built, the ko files of vpu will be generated to the path below :  
**linux_kernel/drivers/media/platform/phytium-vpu/vxd/vxd.ko  
linux_kernel/drivers/media/platform/phytium-vpu/mem_man/img_mem.ko**  
These two ko files should be copied to the path **/system/lib64/media/plugins/** of system partition on development board, when you burn the image.

# Burn Images
## 4.1 Hard disk partition
Prepare a hard disk of SATA,clean all the partitions on it.Under a linux environment, use the fdisk command to create four partitions on the disk. The order of partitions should be boot/system/vendor/userdata. The size of each partition can be set according to the actual situation, for example:500M/3G/500M/10G.  
p1 500MB for BOOT/EFI  
p2 3GB for system  
p3 500MB for vendor  
p4 10G for data  
>You can search for the detailed use of the fdisk command by Baidu and Google, or refer to the contents of disk partition in [Phytium embedded linux user guid](https://gitee.com/phytium_embedded/phytium-sdk/blob/master/%E9%A3%9E%E8%85%BE%E5%B5%8C%E5%85%A5%E5%BC%8FLinux%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8C.pdf).
## 4.2 Burn system/vendor/userdata partitions
Use mkfs tool to format these three partitions as ext4. 
```
sudo mkfs.ext4 sdx2
sudo mkfs.ext4 sdx3
sudo mkfs.ext4 sdx4
```
>**!!! Notice:**  
Use command df to verify the "x" in sdx, it might be a\b\c before you format the disk partitions.

Use command dd to burn the images, which is produced in chapter[3.1 Build OpenHarmony source code](#31-build-openharmony-source-code), to the corresponding disk partition.
```
sudo dd if=system.img of=/dev/sdx2 bs=1M
sudo dd if=vendor.img of=/dev/sdx3 bs=1M
```
>**!!! Notice:** If the system booting is blocked, try to format data partition(**sudo mkfs.ext4 sdx4**)

## 4.3. Burn boot partition   
According to the different boot mode, the burning method of boot partition will be different.There are two kinds of boot mode:boot by Uboot and boot by UEFI.
### 4.3.1 Boot by Uboot
1. Format boot partition as ext4
```
sudo mkfs.ext4 sdx1
```
2. Mount boot partition
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3. Copy the linux kernel images, device tree files, which are got in chapter [3.2 Build linux kernel](#32-build-linux-kernel), and ramdisk image which is got in chapter [3.1 Build OpenHarmony source code](#31-build-openharmony-source-code), to boot partition.  
```
sudo cp linux_kernel/arch/arm64/boot/Image ~/disk/
sudo cp linux_kernel/arch/arm64/boot/dts/phytium/d2000-devboard-dsk.dtb ~/disk/
sudo cp $PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img ~/disk/
sync
```
>If you have the linux kernel source code, you can get all files to burn in linux kernel output files. Get more details from README in linux kernel source code.
4. Unmount boot partition
```
sudo umount ~/disk
```
### 4.3.2 Boot by UEFI
1) Format boot partition as EFI
```
sudo mkfs.vfat sdx1
```
2) Mount boot partition
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3) Copy the linux kernel images, device tree files , EFI files, which are got in chapter [3.2 Build linux kernel](#32-build-linux-kernel), and ramdisk image which is got in chapter [3.1 Build OpenHarmony source code](#31-build-openharmony-source-code), to boot partition.
```
sudo cp linux_kernel/arch/arm64/boot/Image ~/disk/
sudo cp linux_kernel/arch/arm64/boot/dts/phytium/d2000-devboard-dsk.dtb ~/disk/
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/EFI/ ~/disk/ -r
sudo cp $PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img ~/disk/
sync
```
>If you have the linux kernel source code, you can get all files to burn in linux kernel output files. Get more details from README in linux kernel source code.

You can modify ~/disk/EFI/BOOT/grub.cfg to change boot parameters, Generally, use the default startup parameters.

4) Unmount boot partition
```
sudo umount ~/disk
```
## 4.4 Image packaging tool
In contrast to the aforementioned burning process, we also provide an image packaging tool that allows you to package several partitions of img files into a single image, and then burn the packaged image to disk at once through the dd or winddows burning tool, without partitioning the disk.  
Tool location: 
```
device_board_phytium/d2000/tools/generate_image/generate_image.sh
```
For details on how to use and configure the tool, please refer to the instructions at the beginning of the script file.
# Boot device

Connect the burnt SATA hard disk and debug serial port cable to the d2000 EVB board.The baud rate of the serial port debugging tool of the host computer is set to 115200, and then power on the  d2000 EVB board.
## 5.1 Boot by Uboot  
Press Enter in the serial port debugging tool window, after power on, and you will get into Uboot environment, set Uboot environment parameters and send uboot start command.
```
setenv bootargs console=ttyAMA1,115200 earlycon=p1011,0x28001000 root=/dev/ram0 elevator=deadline rootwait rw loglevel=6 hardware=d2000 rootfstype=ext4 initrd=0x93000000,90M
setenv bootcmd "ext4load scsi 0:1 0x90100000 d2000-devboard-dsk.dtb;ext4load scsi 0:1 0x90200000 Image;ext4load scsi 0:1 0x93000000 ramdisk.img;booti 0x90200000 - 0x90100000"
saveenv
boot
```
## 5.2 Boot by UEFI
Boot parameters have already been write in the boot partition, so it will directly start system after being powered on.

# Maintainer email 
Phytium Technology Co., Ltd  
zhangjianwei@phytium.com.cn  
tangkaiwen@phytium.com.cn  
xiayan1086@phytium.com.cn  
libowen1180@phytium.com.cn  
chenzigui1762@phytium.com.cn