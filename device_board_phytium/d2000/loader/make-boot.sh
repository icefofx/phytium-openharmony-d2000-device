#!/bin/bash

set -e
IMAGE_SIZE=64  # 64M
IMAGE_BLOCKS=4096


function make_boot_image()
{
        blocks=${IMAGE_BLOCKS}
        block_size=$((${IMAGE_SIZE} * 1024 * 1024 / ${blocks}))
        echo "blocks = ${blocks}  block_size ${block_size}"

        return $?
}

cd ${BOOT_LINUX}
make_boot_image
