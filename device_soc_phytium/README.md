# phytium hardware组件

#### 简介
### 1.1 芯片简介
D2000芯片简介参考[官网信息](https://phytium.com.cn/article/721)。

### 1.2 仓库简介
媒体、南向接口实现、框架及对接层库目录。

###  1.3 约束

目前支持d2000

## 目录

```
├── common
│   └── hal
│       └── usb
├── d2000
│   ├── hardware
│   │   ├── display
│   │   ├── gpu
│   │   ├── vpu
│   │   └── ...
│   └── ...
└── ...
```
## 相关仓

* [device/board/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/device_board_phytium)
* [vendor/phytium](https://gitee.com/phytium_embedded/phytium-openharmony-d2000-device/tree/master/vendor_phytium)
