# Phytium-OpenHarmony-D2000-device

## 介绍
该项目介绍，如何在飞腾信息科技有限公司的 Phytium D2000 + X100 DEV硬件平台上运行 OpenHarmony 标准系统（[ver 3.2Release](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-release.md)）。  
支持X100提供的视频解码硬件加速，以及图形显示硬件加速。  
支持Linux kernel 5.10。

# 搭建开发环境
## 1.1 硬件环境
准备一台装有ubuntu20.04系统X86主机，内存最低配置要求16G。 
## 1.2 下载repo脚本文件
1. 注册码云[gitee](https://gitee.com/signup?redirect_to_url=%2Fdashboard)账号。

2. 注册码云SSH公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)。

3. 安装[git客户端](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git)和[git-lfs](https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading)并配置用户信息。
```
git config --global user.name "your-name"
git config --global user.email "your-email-address"
git config --global credential.helper store
```
4. 安装码云repo工具，可以执行如下命令。
```
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```
>注：如果没有访问/usr/local/bin/目录的权限，可将repo下载至其他目录，并将其配置到环境变量。
## 1.3 获取OpenHarmony标准系统源码
通过repo + ssh 下载源码。
```
export WORK_SPACE=/home/xxx/workspace #替换成自己定义的workspace路径
export PROJ_ROOT=$WORK_SPACE/OpenHarmony
mkdir $WORK_SPACE
mkdir $PROJ_ROOT
cd &PROJ_ROOT
repo init -u https://gitee.com/openharmony/manifest -b OpenHarmony-3.2-Release --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
## 1.4 获取编译工具链
使用安装包方式获取编译工具链。
```
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby
```
## 1.5 执行prebuilts
进入OpenHarmony源码根目录，执行脚本，安装编译器及二进制工具。
```
cd &PROJ_ROOT
bash build/prebuilts_download.sh
```

# 飞腾设备代码下载与整合
## 2.1 下载phytium device源码
创建一个本地目录，然后将phytium device源码下载到该目录。
```
export PHY_DEV=$WORK_SPACE/phytium_device
mkdir $PHY_DEV
cd $PHY_DEV
git clone git@gitee.com:phytium_embedded/phytium-openharmony-d2000-device.git
```
## 2.2 整合phytiym device源码
1. 将获取到的device源码分别放入OpenHarmony的device对应的目录
```
cp $PHY_DEV/device_board_phytium $PROJ_ROOT/device/board/phytium -r
cp $PHY_DEV/device_soc_phytium $PROJ_ROOT/device/soc/phytium -r
cp $PHY_DEV/vendor_phytium $PROJ_ROOT/vendor/phytium -r
```
2. 执行$PHY_DEV/auto_patch.sh，将device_board_phytium/d2000/patch/中的patch打入到OpenHarmony对应仓库中。
```
$PHY_DEV/auto_patch.sh $PROJ_ROOT
```

# 代码编译
## 3.1 编译OpenHarmony源码  
```
cd $PROJ_ROOT
./build.sh --product-name d2000 --ccache --target-cpu arm64
```
 编译成功提示:
```
post_process
=====build d2000 successful.
```
编译生成的文件
```
$PROJ_ROOT/out/d2000/packages/phone/images/system.img
$PROJ_ROOT/out/d2000/packages/phone/images/vendor.img
$PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img
```
## 3.2 编译 Linux kernel
请通过文末提供的邮件联系phytium公司获取kernel源码，并参考kernel源码中的README，编译出相关文件。
>**！！！注意：**  
vpu是作为Linux Kernel的modules的方式进行编译的，内核代码修改更新时，需要同步更新vpu相关的ko文件。  
linux kernel源码编译后，vpu的ko文件生成的位置：  
linux_kernel/drivers/media/platform/phytium-vpu/vxd/vxd.ko  
linux_kernel/drivers/media/platform/phytium-vpu/mem_man/img_mem.ko  
镜像烧写时，将这两个ko文件拷贝到开发板的system分区：  
/system/lib64/media/plugins/

# 镜像烧写
## 4.1 硬盘分区
准备一块SATA硬盘，删除原有分区后，在linux下，使用fdsik命令分区，创建4个分区，依次为boot，system，vendor，userdata，根据实际情况设定，比如可以设定为500M，3G，500M，10G。  
p1 500MB for BOOT/EFI  
p2 3GB for system  
p3 500MB for vendor  
p4 10G for data  
>fdisk命令详细使用方法可自行百度谷歌，或者参考[飞腾嵌入式 LINUX 用户
手册](https://gitee.com/phytium_embedded/phytium-sdk/blob/master/%E9%A3%9E%E8%85%BE%E5%B5%8C%E5%85%A5%E5%BC%8FLinux%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8C.pdf)中的磁盘分区部分内容。
## 4.2 烧录system/vendor/userdata分区
将这三个分区并格式化为ext4，sdX中的X烧写之前先用df命令确认一下是多少，可能是b，c...等，这里一定要注意不要烧错。
```
sudo mkfs.ext4 sdx2
sudo mkfs.ext4 sdx3
sudo mkfs.ext4 sdx4
```
>**！！！注意：**  在格式化之前，用df确认需要烧写的分区，也就是sdx中的x，可能是a、b、c。

使用dd命令将[3.1 编译OpenHarmony源码](#31-编译openharmony源码)章节编译生成的镜像文件烧写到对应分区中。
```
sudo dd if=system.img of=/dev/sdx2 bs=1M
sudo dd if=vendor.img of=/dev/sdx3 bs=1M
```
## 4.3. 烧录boot分区   
boot分区的烧录，启动方式不一样，烧录方法不一样，区分为Uboot启动和UEFI启动两种。
### 4.3.1 Uboot启动
1. 将boot分区格式化为ext4
```
sudo mkfs.ext4 sdx1
```
2. 挂载boot分区
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3. 将[3.2 编译 Linux kernel](#32-编译-linux-kernel)章节中生成的Kernel镜像，设备树文件，以及[3.3 编译OpenHarmony源码](#31-编译openharmony源码)章节中生成的ramdisk拷贝到boot分区。  
```
sudo cp linux_kernel/arch/arm64/boot/Image ~/disk/
sudo cp linux_kernel/arch/arm64/boot/dts/phytium/d2000-devboard-dsk.dtb ~/disk/
sudo cp $PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img ~/disk/
sync
```
>如果使用kernel源码编译的文件，参考kernel源码中提供的README。
4. 卸载boot分区
```
sudo umount ~/disk
```
### 4.3.2 UEFI启动
1) 将boot分区格式化为EFI
```
sudo mkfs.vfat sdx1
```
2) 挂载boot分区
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3) 将[3.2 编译 Linux kernel](#32-编译-linux-kernel)章节中生成的Kernel镜像，设备树文件，EFI文件，以及[3.1 编译OpenHarmony源码](#31-编译openharmony源码)章节中生成的ramdisk拷贝到EFI分区。
```
sudo cp linux_kernel/arch/arm64/boot/Image ~/disk/
sudo cp linux_kernel/arch/arm64/boot/dts/phytium/d2000-devboard-dsk.dtb ~/disk/
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/EFI/ ~/disk/ -r
sudo cp $PROJ_ROOT/out/d2000/packages/phone/images/ramdisk.img ~/disk/
sync
```
>如果使用kernel源码编译的文件，参考kernel源码中提供的README。

如果需要修改启动参数，修改~/disk/EFI/BOOT/grub.cfg文件，一般使用默认启动参数的即可。

4) 卸载boot分区
```
sudo umount ~/disk
```
## 4.4 镜像打包工具
区别于前面提到的烧写流程，我们还提供了一个镜像打包工具，可以将几个分区的img文件打包成一个镜像，通过dd或者winddows下的烧写工具将打包后的镜像一次性烧写到磁盘，不需要对磁盘分区。  
工具所在位置：
```
device_board_phytium/d2000/tools/generate_image/generate_image.sh  
```
使用和配置方法详情，请参考脚本文件开头的使用说明。
# 设备启动

将烧写好的SATA硬盘和调试串口线，连接到d2000 DEV板上。上位机的串口调试工具波特率设置为115200，上电开机。  
## 5.1 Uboot启动  
在开机阶段，按回车，设置uboot环境变量
```
setenv bootargs console=ttyAMA1,115200 earlycon=p1011,0x28001000 root=/dev/ram0 elevator=deadline rootwait rw loglevel=6 hardware=d2000 rootfstype=ext4 initrd=0x93000000,90M
setenv bootcmd "ext4load scsi 0:1 0x90100000 d2000-devboard-dsk.dtb;ext4load scsi 0:1 0x90200000 Image;ext4load scsi 0:1 0x93000000 ramdisk.img;booti 0x90200000 - 0x90100000"
saveenv
boot
```
## 5.2 UEFI启动
启动参数已经在烧写EFI分区时已写好，上电后，会直接进入系统。
# 维护者邮箱 
飞腾信息技术有限公司  
zhangjianwei@phytium.com.cn  
tangkaiwen@phytium.com.cn  
xiayan1086@phytium.com.cn  
libowen1180@phytium.com.cn  
chenzigui1762@phytium.com.cn